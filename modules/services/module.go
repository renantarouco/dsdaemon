package services

import (
	"log"
	"net"
	"path/filepath"

	"gitlab.com/dsnescafe/dsdaemon/api"
	"gitlab.com/dsnescafe/dsdaemon/proto"
	"gitlab.com/dsnescafe/dsdaemon/system"
	"google.golang.org/grpc"
)

// Module - Daemon's module for services and instances management.
type Module struct {
	fs                   *system.FileSystem
	net                  *system.Network
	grpcServer           *grpc.Server
	grpcErrorChan        chan error
	serviceMap           map[string]*Service
	instanceMap          map[string]*Instance
	newServiceChan       chan *Service
	newInstanceChan      chan *Instance
	newServiceSubs       []chan<- *api.ServiceContext
	newInstanceSubs      []chan<- *api.InstanceContext
	finishedInstanceSubs []chan<- *api.InstanceContext
}

// NewModule - Service module contructor like.
func NewModule() *Module {
	m := new(Module)
	m.grpcServer = grpc.NewServer()
	m.grpcErrorChan = make(chan error)
	m.serviceMap = map[string]*Service{}
	m.instanceMap = map[string]*Instance{}
	m.newServiceChan = make(chan *Service)
	m.newInstanceChan = make(chan *Instance)
	m.newServiceSubs = []chan<- *api.ServiceContext{}
	m.newInstanceSubs = []chan<- *api.InstanceContext{}
	m.finishedInstanceSubs = []chan<- *api.InstanceContext{}
	return m
}

// Init - Gathers required OS structure like directories.
func (m *Module) Init(fs *system.FileSystem, net *system.Network) error {
	m.fs = fs
	m.net = net
	for _, file := range fs.ListFiles("lib") {
		if err := m.LoadService(file); err != nil {
			log.Println(err.Error())
		}
	}
	proto.RegisterServicesModuleServer(m.grpcServer, m)
	return nil
}

// Run - Module's main thread.
func (m *Module) Run(grpcAddr string) error {
	go m.runGrpcServer(grpcAddr)
	for {
		select {
		case err := <-m.grpcErrorChan:
			return err
		case instance, ok := <-m.newInstanceChan:
			servicePath := m.serviceMap[instance.ctx.ServiceName()].path
			_, serviceFileName := filepath.Split(servicePath)
			err := m.fs.CopyFile("lib", serviceFileName, "run", instance.ctx.Name()+".io")
			if err != nil {
				return err
			}
			logFile, err := m.fs.CreateFile("log", instance.ctx.Name()+".log")
			if err != nil {
				return err
			}
			instance.logFile = logFile
			instance.ctx.Logger = log.New(logFile, "", log.LstdFlags)
		}
	}
}

func (m *Module) Stop() {

}

func (m *Module) runGrpcServer(addr string) {
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		m.grpcErrorChan <- err
		return
	}
	m.grpcErrorChan <- m.grpcServer.Serve(listener)
}

func (m *Module) LoadService(path string) error {
	service, err := LoadService(path)
	if err != nil {
		return err
	}
	m.serviceMap[service.Name()] = service
	log.Println("loaded", service.Name())
	return nil
}

/*
func (m *Module) ServicePath(name string) string {
	return m.serviceMap[name].path
}

// InstanceRoutine - The routine to play when a new instance is launched.
func (m *Module) InstanceRoutine(instance *Instance) {
	errorChan := make(chan error)
	go func() {
		errorChan <- instance.Run()
	}()
	m.totalInstances++
	instance.SetStatus(api.StatusRunning)
	m.notifyInstanceEvent(m.newInstanceSubs, instance.context)
	select {
	case err := <-errorChan:
		if err != nil {
			log.Printf("%s: %s", instance.Name(), err.Error())
			instance.SetStatus(api.StatusError)
		} else {
			instance.SetStatus(api.StatusOk)
		}
	case <-instance.context.KillChan:
		instance.SetStatus(api.StatusKilled)
	}
	log.Print("1")
	instance.Stop()
	m.notifyInstanceEvent(m.finishedInstanceSubs, instance.context)
	log.Print("2")
	serviceName := instance.ServiceName()
	m.ServiceContext(serviceName).InstanceCount--
	log.Print("3")
	m.totalInstances--
	if m.serviceMap[serviceName].ctx.InstanceCount == 0 {
		delete(m.serviceMap, serviceName)
	}
	log.Print("4")
	delete(m.instanceMap, instance.Name())
	instance = nil
	log.Print("5")
}

// KillInstance - Performs routine for killing an instance and notifies interested parts in this event.
func (m *Module) KillInstance(instanceName string) {
	close(m.InstanceContext(instanceName).KillChan)
}

// Services - Gets all services mapped in the runtime.
func (m *Module) Services() []string {
	services := []string{}
	for serviceName := range m.serviceMap {
		services = append(services, serviceName)
	}
	return services
}

// ServiceContext - Gives the instance count for a particular service by its name.
func (m *Module) ServiceContext(serviceName string) *api.ServiceContext {
	return m.serviceMap[serviceName].ctx
}

// Instances - Returns the instances names mapped in the runtime.
func (m *Module) Instances() []string {
	instances := []string{}
	for instanceName := range m.instanceMap {
		instances = append(instances, instanceName)
	}
	return instances
}

// InstanceContext - Gets the status of an instance given its name.
func (m *Module) InstanceContext(instanceName string) *api.InstanceContext {
	return m.instanceMap[instanceName].context
}

// SubscribeToNewInstance - Registers a channel to be notified when this module starts a new instance.
func (m *Module) SubscribeToNewInstance(channel chan<- *api.InstanceContext) {
	m.newInstanceSubs = append(m.newInstanceSubs, channel)
}

// SubscribeToFinishedInstance - Registers a channel to be notified when this module kill an instance.
func (m *Module) SubscribeToFinishedInstance(channel chan<- *api.InstanceContext) {
	m.newInstanceSubs = append(m.finishedInstanceSubs, channel)
}

func (m *Module) notifyInstanceEvent(subscribers []chan<- *api.InstanceContext, instance *api.InstanceContext) {
	for _, c := range subscribers {
		c <- instance
	}
}
*/
