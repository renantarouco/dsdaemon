package services

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/dsnescafe/dsdaemon/proto"
)

// PushService - Pushes a SO file in the daemon's registry.
func (m *Module) PushService(
	stream proto.ServicesModule_PushServiceServer) error {
	var fileName string
	var bytesChan chan []byte
	var errorChan chan error
	for {
		select {
		case err := <-errorChan:
			return err
		default:
			chunk, err := stream.Recv()
			if err != nil {
				if err == io.EOF {
					close(bytesChan)
					log.Println("finished push", fileName)
					filePath := fmt.Sprintf("%s/%s", m.fs.DirPath("lib"), fileName)
					if err := m.LoadService(filePath); err != nil {
						return err
					}
					return stream.SendAndClose(&proto.PushServiceResponse{})
				}
				return err
			}
			if chunk.Header.GetChunkNumber() == 1 {
				log.Println("started push", fileName)
				fileName = chunk.Header.GetFileName()
				bytesChan, errorChan = m.fs.TransferFileTo("lib", fileName)
			}
			bytesChan <- chunk.GetChunk()
		}
	}
}

// ListServices - Lists all services present in the daemon's registry.
func (m *Module) ListServices(
	req *proto.ListServicesRequest,
	stream proto.ServicesModule_ListServicesServer) error {
	serviceDesc := &proto.ServiceDescriptor{}
	for _, service := range m.serviceMap {
		serviceDesc.Name = service.Name()
		serviceDesc.Ports = service.Ports()
		file, err := os.Open(service.path)
		if err != nil {
			return err
		}
		fileInfo, err := file.Stat()
		serviceDesc.Size = uint64(fileInfo.Size())
		if err = stream.Send(serviceDesc); err != nil {
			return err
		}
	}
	return nil
}

// RemoveService - Uninstall a service from de registry.
func (m *Module) RemoveService(
	ctx context.Context,
	req *proto.RemoveServiceRequest) (*proto.RemoveServiceResponse, error) {
	name := req.GetName()
	if _, ok := m.serviceMap[name]; ok {
		err := os.Remove(m.serviceMap[name].path)
		if err != nil {
			return &proto.RemoveServiceResponse{}, err
		}
		delete(m.serviceMap, name)
	}
	return &proto.RemoveServiceResponse{}, nil
}

// PullService - Downloads the SO file of a service to the host that called.
func (m *Module) PullService(
	req *proto.PullServiceRequest,
	stream proto.ServicesModule_PullServiceServer) error {
	_, fileName := filepath.Split(m.serviceMap[req.GetName()].path)
	chunk := &proto.ServiceChunk{
		Header: &proto.ChunkHeader{
			FileName: fileName,
		},
	}
	bytesChan, errorChan := m.fs.TransferFileFrom("lib", fileName)
	chunkNumber := 1
	log.Println("started pull")
	for {
		select {
		case err := <-errorChan:
			return err
		case bytes, ok := <-bytesChan:
			if ok {
				chunk.Chunk = bytes
				chunk.Header.ChunkNumber = uint32(chunkNumber)
				chunkNumber++
				if err := stream.Send(chunk); err != nil {
					return err
				}
			} else {
				log.Println("finished pull")
				return nil
			}
		}
	}
}

func (m *Module) RunInstance(
	ctx context.Context,
	req *proto.RunInstanceRequest) (*proto.RunInstanceResponse, error) {
	serviceName := req.GetServiceName()
	service, ok := m.serviceMap[serviceName]
	if !ok {
		return nil, fmt.Errorf("%s service not loaded", serviceName)
	}
	name := req.GetName()
	if name == "" {
		name = service.Name() + fmt.Sprint(service.instanceCount)
	}
	portsMap := req.GetPortsMap()
	for _, iPort := range service.Ports() {
		hPort, ok := portsMap[iPort]
		if ok {
			if err := m.net.MapPort(iPort, hPort); err != nil {
				return nil, err
			}
		} else {
			return nil, fmt.Errorf("%s port must be mapped", iPort)
		}
	}
	mountPath := fmt.Sprintf("%s/%s", m.fs.DirPath("mnt"), name)
	instance := service.NewInstance(name, portsMap, mountPath)
	m.newInstanceChan <- instance
	return &proto.RunInstanceResponse{}, nil
}

/*
// RunInstance - Runs the routine of a service as an instance.

// ListInstances - Lists all running instances an their status.
func (a *API) ListInstances(
	req *proto.ListInstancesRequest,
	stream proto.ServicesModule_ListInstancesServer) error {
	instanceDesc := &proto.InstanceDescriptor{}
	for _, instance := range a.module.instanceMap {
		instanceDesc.Id = uint32(instance.ID())
		instanceDesc.Name = instance.Name()
		instanceDesc.ServiceName = instance.ServiceName()
		instanceDesc.Status = instance.Status()
		if err := stream.Send(instanceDesc); err != nil {
			log.Println(err.Error())
		}
	}
	return nil
}

// KillInstance - Kills a running instance.
func (a *API) KillInstance(
	ctx context.Context,
	req *proto.KillInstanceRequest) (*proto.KillInstanceResponse, error) {
	instanceName := req.GetName()
	a.module.KillInstance(instanceName)
	return &proto.KillInstanceResponse{}, nil
}
*/
