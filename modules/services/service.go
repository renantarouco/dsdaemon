package services

import (
	"gitlab.com/dsnescafe/dsdaemon/api"
)

// Service - The structo holding information about a service to be instantiated.
type Service struct {
	path          string
	instanceCount uint
	ctx           api.ServiceContext
}

func (s *Service) Name() string {
	return s.ctx.Name
}

func (s *Service) Ports() []string {
	return s.ctx.Ports
}

// NewInstance - Service's instantiation function.
func (s *Service) NewInstance(name string, portsMap map[string]string, mountPath string) *Instance {
	ctx := api.NewInstanceContext(s.name, name, mountPath, portsMap)
	s.instanceCount++
	return &Instance{ctx: *ctx}
}
