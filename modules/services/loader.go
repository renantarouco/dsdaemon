package services

import (
	"errors"
	"plugin"

	"gitlab.com/dsnescafe/dsdaemon/api"
)

// CheckPluginStructure - Checks if the SO file was compiled with conformace to
// the expected plugin pattern.
func CheckPluginStructure(path string) error {
	// Opens the plugin file.
	pluginSO, err := plugin.Open(path)
	if err != nil {
		return err
	}
	// Looks for the Context exported field.
	ctxSym, err := pluginSO.Lookup("Context")
	if err != nil {
		return err
	}
	// Checks if Context field is a *api.ServiceContext.
	_, ok := ctxSym.(*api.ServiceContext)
	if !ok {
		return errors.New("unexpected type for Context symbol, wanted: *api.ServiceContext")
	}
	// Looks for the Run function.
	runSym, err := pluginSO.Lookup("Run")
	if err != nil {
		return err
	}
	_, ok = runSym.(func(*api.InstanceContext) error)
	if !ok {
		return errors.New("unexpected type for Run symbol, wanted: Run(*api.InstanceContext) error")
	}
	// Looks for the Stop function.
	stopSym, err := pluginSO.Lookup("Stop")
	if err != nil {
		return err
	}
	_, ok = stopSym.(func(*api.InstanceContext))
	if !ok {
		return errors.New("unexpected type for Stop symbol, wanted: Stop(*api.InstanceContext)")
	}
	return nil
}

// LoadService - Loads the SO file structure in the runtime environment.
func LoadService(path string) (*Service, error) {
	if err := CheckPluginStructure(path); err != nil {
		return nil, err
	}
	pluginSO, err := plugin.Open(path)
	if err != nil {
		return nil, err
	}
	ctxSym, _ := pluginSO.Lookup("Context")
	ctx, _ := ctxSym.(*api.ServiceContext)
	return &Service{path: path, instanceCount: 0, ctx: *ctx}, nil
}

func LoadInstance(path string) (*Instance, error) {
	if err := CheckPluginStructure(path); err != nil {
		return nil, err
	}
	pluginSO, err := plugin.Open(path)
	if err != nil {
		return nil, err
	}
	ctxSym, _ := pluginSO.Lookup("Context")
	sCtx, _ := ctxSym.(*api.ServiceContext)
	// Looks for the Run function.
	runSym, err := pluginSO.Lookup("Run")
	runFunc, _ := runSym.(func(*api.InstanceContext) error)
	// Looks for the Stop function.
	stopSym, err := pluginSO.Lookup("Stop")
	stopFunc, _ := stopSym.(func(*api.InstanceContext))
	return &Instance{
		killChan: make(chan struct{})
	}, nil
}

/*
// Loader - Struct used to load and check SO files with plugin pattern conformance.
type Loader struct{}

// NewLoader - Service loader contructor like.
func NewLoader() *Loader {
	l := new(Loader)
	return l
}

func (l *Loader) LoadServices(path string) map[string]*Service {
	serviceMap := map[string]*Service{}
	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(path) != ".so" {
			return nil
		}
		service, err := l.LoadService(path)
		if err != nil {
			return nil
		}
		serviceMap[service.Name()] = service
		return nil
	})
	return serviceMap
}

*/
