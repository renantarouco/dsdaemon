package services

import (
	"os"

	"gitlab.com/dsnescafe/dsdaemon/api"
)

// Instance - Daemon's representation of an instantiated service.
type Instance struct {
	killChan chan struct{}
	logFile  *os.File
	ctx      *api.InstanceContext
	Run      func(*api.InstanceContext) error
	Stop     func(*api.InstanceContext)
}

// // Run - The main function of the instance.
// func (i *Instance) Run() error {
// 	return i.api.Run(i.context)
// }

// // Stop - The Stop function interface.
// func (i *Instance) Stop() {
// 	i.api.Stop(i.context)
// }
