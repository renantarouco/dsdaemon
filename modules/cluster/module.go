package cluster

import (
	"gitlab.com/dsnescafe/dsdaemon/api"

	"go.etcd.io/etcd/embed"
)

type Module struct {
	etcd             *embed.Etcd
	api              *API
	newInstanceChan  chan *api.InstanceContext
	killInstanceChan chan *api.InstanceContext
}

// NewModule - Constructor like for a cluster module.
func NewModule() *Module {
	m := new(Module)
	return m
}

/*
// Init - Starts the necessary variables for running the etcd instance and api's gRPC server.
func (m *Module) Init(rootPath string, clientUrls, peerUrls []string, serv *services.Module) error {
	config := embed.NewConfig()
	name, err := os.Hostname()
	if err != nil {
		return err
	}
	config.Name = name
	config.Dir = fmt.Sprintf("%s/etcd", rootPath)
	listenClient, listenPeer := []url.URL{}, []url.URL{}
	for _, urlStr := range clientUrls {
		listenClient = append(listenClient, url.URL{Scheme: "http", Host: urlStr})
	}
	for _, urlStr := range peerUrls {
		listenPeer = append(listenPeer, url.URL{Scheme: "http", Host: urlStr})
	}
	config.LCUrls = listenClient
	config.LPUrls = listenPeer
	config.InitialCluster = config.InitialClusterFromName(name)
	etcd, err := embed.StartEtcd(config)
	if err != nil {
		return err
	}
	select {
	case <-etcd.Server.ReadyNotify():
		log.Println("etcd server is ready")
	case <-time.After(60 * time.Second):
		etcd.Server.Stop()
		return errors.New("etcd server took too long to start")
	}
	m.etcd = etcd
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"localhost:2379"},
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		return err
	}
	defer client.Close()
	for _, serviceName := range serv.Services() {
		RegisterService(client, serv.ServiceContext(serviceName))
	}
	for _, instanceName := range serv.Instances() {
		instance := serv.InstanceContext(instanceName)
		RegisterInstance(client, instance)
		IncrementServiceInstanceCount(client, instance)
	}
	m.api = new(API)
	m.newInstanceChan = make(chan *api.InstanceContext)
	m.killInstanceChan = make(chan *api.InstanceContext)
	serv.SubscribeToNewInstance(m.newInstanceChan)
	serv.SubscribeToFinishedInstance(m.killInstanceChan)
	return nil
}

// Run - Runs the module listening for errors in the etcd and API routine.
func (m *Module) Run(grpcAddr string) error {
	go func() {
		for {
			instance, ok := <-m.newInstanceChan
			if !ok {
				break
			}
			client, err := clientv3.New(clientv3.Config{
				Endpoints:   []string{"localhost:2379"},
				DialTimeout: 5 * time.Second,
			})
			if err != nil {
				log.Print(err.Error())
			}
			RegisterInstance(client, instance)
			IncrementServiceInstanceCount(client, instance)
			client.Close()
		}
	}()
	go func() {
		for {
			instance, ok := <-m.killInstanceChan
			if !ok {
				break
			}
			client, err := clientv3.New(clientv3.Config{
				Endpoints:   []string{"localhost:2379"},
				DialTimeout: 5 * time.Second,
			})
			if err != nil {
				log.Print(err.Error())
			}
			UnregisterInstance(client, instance)
			DecrementServiceInstanceCount(client, instance)
			client.Close()
		}
	}()
	var err error
	select {
	case err = <-m.etcd.Err():
	case err = <-m.api.Run(m, grpcAddr):
	}
	return err
}

// Stop - Stops etcd and API routine.
func (m *Module) Stop() {
	m.etcd.Close()
	m.api.Stop()
}
*/
