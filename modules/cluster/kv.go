package cluster

/*
// RegisterService - Stores services related keys.
func RegisterService(client *clientv3.Client, service *api.ServiceContext) error {
	baseKey := fmt.Sprintf("services:service_map:%s", service.Name)
	key := fmt.Sprintf("%s:export", baseKey)
	_, err := client.Put(context.Background(), key, service.Export)
	if err != nil {
		return err
	}
	key = fmt.Sprintf("%s:instance_count", baseKey)
	_, err = client.Put(context.Background(), key, fmt.Sprint(service.InstanceCount))
	if err != nil {
		return err
	}
	return nil
}

// RegisterInstance - Registers the instance associated keys.
func RegisterInstance(client *clientv3.Client, instance *api.InstanceContext) error {
	baseKey := "services:instance_map"
	key := fmt.Sprintf("%s:%s:status", baseKey, instance.Name)
	_, err := client.Put(context.Background(), key, instance.Status)
	if err != nil {
		return err
	}
	for iPort, hPort := range instance.PortsMap {
		key = fmt.Sprintf("%s:%s:ports_map:%s", baseKey, instance.Name, iPort)
		_, err = client.Put(context.Background(), key, hPort)
		if err != nil {
			return err
		}
	}
	return nil
}

// UnregisterInstance - Deletes instances associated keys.
func UnregisterInstance(client *clientv3.Client, instance *api.InstanceContext) error {
	keyPrefix := fmt.Sprintf("services:instance_map:%s", instance.Name)
	_, err := client.Delete(context.Background(), keyPrefix, clientv3.WithPrefix())
	if err != nil {
		return err
	}
	return nil
}

func IncrementServiceInstanceCount(client *clientv3.Client, instance *api.InstanceContext) error {
	baseKey := fmt.Sprintf("services:service_map:%s", instance.ServiceName)
	key := fmt.Sprintf("%s:instance_count", baseKey)
	gr, err := client.Get(context.Background(), key)
	if err != nil {
		return err
	}
	instanceCount, err := strconv.ParseUint(string(gr.Kvs[0].Value), 0, 64)
	if err != nil {
		return err
	}
	instanceCount++
	_, err = client.Put(context.Background(), key, fmt.Sprint(instanceCount))
	if err != nil {
		return err
	}
	return nil
}

func DecrementServiceInstanceCount(client *clientv3.Client, instance *api.InstanceContext) error {
	baseKey := fmt.Sprintf("services:service_map:%s", instance.ServiceName)
	key := fmt.Sprintf("%s:instance_count", baseKey)
	gr, err := client.Get(context.Background(), key)
	if err != nil {
		return err
	}
	instanceCount, err := strconv.ParseUint(string(gr.Kvs[0].Value), 0, 64)
	if err != nil {
		return err
	}
	instanceCount--
	_, err = client.Put(context.Background(), key, fmt.Sprint(instanceCount))
	if err != nil {
		return err
	}
	return nil
}
*/
