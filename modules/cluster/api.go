package cluster

import (
	"context"
	"net"

	"gitlab.com/dsnescafe/dsdaemon/proto"
	"google.golang.org/grpc"
)

// API - Type holding reference to the module it works on and the gRPC server itself.
type API struct {
	module     *Module
	grpcServer *grpc.Server
}

// Run - Runs the gRPC server to listen to API calls.
func (a *API) Run(module *Module, grpcAddr string) chan error {
	a.module = module
	a.grpcServer = grpc.NewServer()
	errorChan := make(chan error)
	proto.RegisterClusterModuleServer(a.grpcServer, a)
	listener, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		errorChan <- err
		close(errorChan)
		return errorChan
	}
	go func() {
		errorChan <- a.grpcServer.Serve(listener)
	}()
	return errorChan
}

// Stop - Stops the gRPC server.
func (a *API) Stop() {
	a.grpcServer.Stop()
}

// gRPC implementations.

// JoinCluster - Command to tell daemon to join an existing cluster.
func (a *API) JoinCluster(
	ctx context.Context,
	req *proto.JoinClusterRequest) (*proto.JoinClusterResponse, error) {
	return nil, nil
}

// RegisterNode - Command to register a node to the host's cluster.
func (a *API) RegisterNode(
	ctx context.Context,
	req *proto.RegisterNodeRequest) (*proto.RegisterNodeResponse, error) {
	return nil, nil
}
