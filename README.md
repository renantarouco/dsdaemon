# dsdaemon

## dsctl

dsdaemon's command-line interface.

### Synopsis

dsdaemon's command-line interface.

### Options

```
  -d, --daemon string   Daemon's IP to be managed. (default "127.0.0.1")
  -h, --help            help for dsctl
```

## dsctl service

Manages daemon's services.

### Synopsis

Manages daemon's services.

## dsctl service list

Lists all services installed in the daemon.

### Synopsis

Lists all services installed in the daemon.

```
dsctl service list [flags]
```

## dsctl service push

Installs a service in the daemon's repository.

### Synopsis

Installs a service in the daemon's repository.

```
dsctl service push [flags]
```

## dsctl service pull

Downloads a service from the daemon's repository.

### Synopsis

Downloads a service from the daemon's repository.

```
dsctl service pull [flags]
```

### Options

```
  -o, --output string   Output directory do download the service file. (default ".")
```

## dsctl service remove

Removes services from the daemon's repository.

### Synopsis

Removes services from the daemon's repository.

```
dsctl service remove [flags]
```

## dsctl doc

Generates ctl documentation for a given file format.

### Synopsis

Generates ctl documentation for a given file format.

```
dsctl doc [flags]
```

### Options

```
  -f, --file-format string   Output's format [md, rst, man] (required)
  -h, --help                 help for doc
  -o, --output string        Output directory. (default ".")
```
