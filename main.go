package main

import "log"

func main() {
	daemon := NewDaemon()
	if err := daemon.Init(); err != nil {
		log.Fatal(err.Error())
	}
	if err := daemon.Run(); err != nil {
		log.Fatal(err.Error())
	}
}
