package api

type ServiceContext struct {
	Name  string
	Ports []string
}
