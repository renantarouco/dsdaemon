package api

import (
	"fmt"
	"log"
)

const (
	// StatusCreated - The moment the instance is created.
	StatusCreated = "created"
	// StatusRunning - As soon as the Run() function is called.
	StatusRunning = "running"
	// StatusFinished - When the Run() function returns, must check for error.
	StatusFinished = "finished"
	// StatusKilled - When the user kills the instance.
	StatusKilled = "killed"
)

// InstanceContext - All information about and instance to be used by the daemon
// and the instance itself.
type InstanceContext struct {
	// User defined.
	serviceName string
	name        string
	mountPath   string
	portsMap    map[string]string
	// System defined.
	Logger *log.Logger
	status string
	err    error
}

func NewInstanceContext(serviceName, name, mountPath string, portsMap map[string]string) *InstanceContext {
	i := new(InstanceContext)
	i.serviceName = serviceName
	i.name = name
	i.mountPath = mountPath
	i.portsMap = portsMap
	i.status = StatusCreated
	return i
}

func (i *InstanceContext) ServiceName() string {
	return i.serviceName
}

func (i *InstanceContext) Name() string {
	return i.name
}

func (i *InstanceContext) Port(iPort string) (string, error) {
	hPort, ok := i.portsMap[iPort]
	if !ok {
		return "", fmt.Errorf("%s port not mapped")
	}
	return hPort, nil
}

func (i *InstanceContext) MountPath() string {
	return i.mountPath
}

func (i *InstanceContext) Status() string {
	return i.status
}

func (i *InstanceContext) Err() error {
	return i.err
}
