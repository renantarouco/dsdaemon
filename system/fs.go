package system

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

type FileSystem struct {
	rootPath string
	dirsMap  map[string]string
	logFile  *os.File
}

func NewFileSystem() *FileSystem {
	fs := new(FileSystem)
	return fs
}

func (fs *FileSystem) Init(rootPath string) error {
	// Create root path.
	err := fs.CreateDirIfNotExists(rootPath)
	if err != nil {
		return err
	}
	fs.rootPath = rootPath
	// Create directory tree for the daemon.
	fs.dirsMap = map[string]string{}
	dirs := []string{"lib", "run", "mnt", "log"}
	for _, dir := range dirs {
		dirPath := fmt.Sprintf("%s/%s", rootPath, dir)
		err := fs.CreateDirIfNotExists(dirPath)
		if err != nil {
			return err
		}
		fs.dirsMap[dir] = dirPath
	}
	// Creates the general daemon log file.
	logFile, err := os.Create(fs.rootPath + "/daemon.log")
	if err != nil {
		return err
	}
	// Redirects log for Stdout and the created log file.
	writer := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(writer)
	fs.logFile = logFile
	return nil
}

func (fs *FileSystem) Stop() {
	if err := fs.logFile.Close(); err != nil {
		log.Fatal(err.Error())
	}
}

func (fs *FileSystem) DirPath(dir string) string {
	dirPath, ok := fs.dirsMap[dir]
	if !ok {
		return ""
	}
	return dirPath
}

func (fs *FileSystem) ListFiles(dir string) []string {
	files := []string{}
	filepath.Walk(fs.dirsMap[dir], func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		files = append(files, path)
		return nil
	})
	return files
}

func (fs *FileSystem) CreateDirIfNotExists(path string) error {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(path, os.ModePerm)
		} else {
			return err
		}
	}
	return nil
}

func (fs *FileSystem) CreateFile(dir, fileName string) (*os.File, error) {
	filePath := fmt.Sprintf("%s/%s", fs.dirsMap[dir], fileName)
	file, err := os.Create(filePath)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func (fs *FileSystem) TransferFileTo(dir, name string) (chan []byte, chan error) {
	bytesChan := make(chan []byte)
	errorChan := make(chan error)
	go func() {
		defer close(errorChan)
		path := filepath.Join(fs.dirsMap[dir], name)
		file, err := os.Create(path)
		if err != nil {
			errorChan <- err
			return
		}
		file, err = os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			errorChan <- err
			return
		}
		for {
			chunk, ok := <-bytesChan
			if ok {
				_, err := file.Write(chunk)
				if err != nil {
					rmErr := os.Remove(path)
					if rmErr != nil {
						errorChan <- rmErr
					} else {
						errorChan <- err
					}
					return
				}
				file.Sync()
			} else {
				file.Close()
				return
			}
		}
	}()
	return bytesChan, errorChan
}

func (fs *FileSystem) TransferFileFrom(dir, name string) (chan []byte, chan error) {
	bytesChan := make(chan []byte)
	errorChan := make(chan error)
	go func() {
		defer close(bytesChan)
		defer close(errorChan)
		path := filepath.Join(fs.dirsMap[dir], name)
		_, err := os.Stat(path)
		if err != nil {
			log.Println(err.Error())
			errorChan <- err
			return
		}
		file, err := os.Open(path)
		if err != nil {
			errorChan <- err
			return
		}
		chunkSize := 64 * 1024
		buffer := make([]byte, chunkSize)
		for {
			bytesRead, err := file.Read(buffer)
			if err != nil {
				if err == io.EOF {
					file.Close()
					break
				}
				errorChan <- err
				return
			}

			bytesChan <- buffer[:bytesRead]
		}
	}()
	return bytesChan, errorChan
}

func (fs *FileSystem) CopyFile(from, srcFile, to, destFile string) error {
	destChan, dErrorChan := fs.TransferFileTo(to, destFile)
	srcChan, sErrorChan := fs.TransferFileFrom(from, srcFile)
	for {
		select {
		case err := <-dErrorChan:
			return err
		case err := <-sErrorChan:
			return err
		case chunk, ok := <-srcChan:
			if ok {
				destChan <- chunk
			} else {
				return nil
			}
		}
	}
}
