package system

import (
	"fmt"
)

type Network struct {
	minPort   uint
	maxPort   uint
	currPort  uint
	usedPorts map[string]string
}

func NewNetwork() *Network {
	net := new(Network)
	return net
}

func (net *Network) Init(minPort, maxPort uint) error {
	net.minPort = minPort
	net.maxPort = maxPort
	net.currPort = minPort - 1
	if err := net.MapPort("9000", "9000"); err != nil {
		return err
	}
	if err := net.MapPort("9001", "9001"); err != nil {
		return err
	}
	if err := net.MapPort("9002", "9002"); err != nil {
		return err
	}
	return nil
}

func (net *Network) Stop() {

}

func (net *Network) MapPort(vPort, rPort string) error {
	_, ok := net.usedPorts[rPort]
	if !ok {
		net.usedPorts[vPort] = rPort
		return nil
	}
	return fmt.Errorf("%s port already in use", rPort)
}
