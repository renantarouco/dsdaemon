#!/bin/sh
while IFS= read -r dep
do
    go get -u -v $dep
done < "deps.txt"
