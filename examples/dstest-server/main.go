package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/dsnescafe/dsdaemon/api"
)

// Declaration

var Context = api.ServiceContext{
	Name:  "dstest-server",
	Ports: []string{"8000", "8001", "8002"},
}

// Implementation

var counter = 0

func Run(ctx *api.InstanceContext) error {
	time.Sleep(2 * time.Minute)
	counter++
	return nil
}

func Stop(ctx *api.InstanceContext) {
	log.Print("finished instance", ctx.Name)
	switch ctx.Status {
	case api.StatusFinished:
		if ctx.Error != nil {
			log.Print("error")
		}
		log.Print("ok")
	case api.StatusKilled:
		log.Print("killed")
	}
	log.Println("counter", fmt.Sprint(counter))
}
