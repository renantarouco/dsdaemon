package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/dsnescafe/dsdaemon/modules/services"
	"gitlab.com/dsnescafe/dsdaemon/proto"
	"gitlab.com/dsnescafe/dsdaemon/system"
	"google.golang.org/grpc"
)

// Daemon - Structure that holds the main modules of the daemon app.
type Daemon struct {
	// Native
	fs         *system.FileSystem
	net        *system.Network
	grpcServer *grpc.Server
	// Modules
	serv *services.Module
	//cluster *cluster.Module
	// Error Channels
	grpcErrorChan    chan error
	servErrorChan    chan error
	clusterErrorChan chan error
}

// NewDaemon - Contructor like for the daemon app.
func NewDaemon() *Daemon {
	d := new(Daemon)
	// Native
	d.fs = system.NewFileSystem()
	d.net = system.NewNetwork()
	d.grpcServer = grpc.NewServer()
	// Modules
	d.serv = services.NewModule()
	//d.cluster = cluster.NewModule()
	// Error Channels
	d.grpcErrorChan = make(chan error)
	d.servErrorChan = make(chan error)
	d.clusterErrorChan = make(chan error)
	return d
}

// Init - Inits daemon file system, network, grpc and modules.
func (d *Daemon) Init() error {
	if err := d.fs.Init(os.Getenv("HOME") + "/.ds"); err != nil {
		return err
	}
	if err := d.net.Init(11000, 12000); err != nil {
		return err
	}
	proto.RegisterDaemonServer(d.grpcServer, d)
	return nil
}

// Run - Daemon's main procedure.
func (d *Daemon) Run() error {
	go d.runGrpcServer(":9000")
	go d.runServicesModule(":9001")
	log.Print("daemon started")
	for {
		select {
		case err := <-d.grpcErrorChan:
			if err != nil {
				return fmt.Errorf("%s: %s", "grpc", err.Error())
			}
		case err := <-d.servErrorChan:
			if err != nil {
				return fmt.Errorf("%s: %s", "services", err.Error())
			}
		case err, ok := <-d.clusterErrorChan:
			if ok {
				if err != nil {
					log.Printf("%s: %s", "cluster", err.Error())
				}
			}
		}
	}
}

func (d *Daemon) Stop() {
	d.fs.Stop()
	d.net.Stop()
	d.grpcServer.GracefulStop()
	d.serv.Stop()
	//d.cluster.stop()
	close(d.grpcErrorChan)
	close(d.servErrorChan)
	//close(d.clusterErrorChan)
}

func (d *Daemon) runGrpcServer(addr string) {
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		d.grpcErrorChan <- err
		return
	}
	d.grpcErrorChan <- d.grpcServer.Serve(listener)
}

func (d *Daemon) runServicesModule(grpcAddr string) {
	if err := d.serv.Init(d.fs, d.net); err != nil {
		d.servErrorChan <- err
		return
	}
	d.servErrorChan <- d.serv.Run(grpcAddr)
}

/*
func (d *Daemon) runClusterModule(clientUrls, peerUrls []string) {
	if d.cluster != nil {
		d.clusterErrorChan <- errors.New("cluster already running, stop it first")
		return
	}
	d.cluster = cluster.NewModule()
	if err := d.cluster.Init(d.rootPath, clientUrls, peerUrls, d.serv); err != nil {
		d.clusterErrorChan <- err
		return
	}
	d.clusterErrorChan <- d.cluster.Run(":9002")
}

func (d *Daemon) stopClusterModule() {
	if d.cluster != nil {
		d.cluster.Stop()
		d.cluster = nil
	}
}

// gRPC implementations

// StartClusterModule - Starts the cluster functionality in the daemon.
func (d *Daemon) StartClusterModule(
	ctx context.Context,
	req *proto.StartClusterModuleRequest) (*proto.StartClusterModuleResponse, error) {
	clientURLs := req.GetClientUrls()
	peerURLs := req.GetPeerUrls()
	go d.runClusterModule(clientURLs, peerURLs)
	return &proto.StartClusterModuleResponse{}, nil
}

// StopClusterModule - Stops the daemon's cluster module.
func (d *Daemon) StopClusterModule(
	ctx context.Context,
	req *proto.StopClusterModuleRequest) (*proto.StopClusterModuleResponse, error) {
	d.stopClusterModule()
	return &proto.StopClusterModuleResponse{}, nil
}
*/
