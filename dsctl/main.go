package main

import (
	"gitlab.com/dsnescafe/dsdaemon/dsctl/cmd"
)

func main() {
	cmd.Execute()
}
