package cmd

import (
	"fmt"
	"os"

	"gitlab.com/dsnescafe/dsdaemon/dsctl/util"
	"gitlab.com/dsnescafe/dsdaemon/proto"
	"google.golang.org/grpc"

	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/dsctl/cmd/services"
)

const (
	ctlPort = "9000"
)

var (
	conn   *grpc.ClientConn
	client proto.DaemonClient
)

func init() {
	rootCmd.PersistentFlags().StringP(
		"daemon",
		"d",
		"127.0.0.1",
		"Daemon's IP to be managed.")
	rootCmd.AddCommand(startClusterCmd)
	rootCmd.AddCommand(services.ServiceCmd)
}

var rootCmd = &cobra.Command{
	Use:     "dsctl",
	Short:   "dsdaemon's command-line interface.",
	Version: "0.0.0",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		daemonIP := cmd.Flag("daemon").Value.String()
		var err error
		conn, err = grpc.Dial(daemonIP+":"+ctlPort, grpc.WithInsecure())
		util.Check(err)
		client = proto.NewDaemonClient(conn)
		util.Check(err)
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		conn.Close()
	},
}

// Execute - Function to be called in main.go.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
