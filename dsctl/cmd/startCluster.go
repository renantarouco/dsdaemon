package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	startClusterCmd.Flags().StringSliceP(
		"client",
		"c",
		[]string{"localhost:2379"},
		"The etcd address to listen for client requests.")
	startClusterCmd.Flags().StringSliceP(
		"peer",
		"p",
		[]string{"localhost:2380"},
		"The etcd address to listen for peer requests.")
}

var startClusterCmd = &cobra.Command{
	Use:   "start-cluster",
	Short: "Starts daemon's cluster functionality.",
	Run: func(cmd *cobra.Command, args []string) {
		// clientURLS, err := cmd.Flags().GetStringSlice("client")
		// util.Check(err)
		// peerURLS, err := cmd.Flags().GetStringSlice("peer")
		// util.Check(err)
		// _, err = client.StartClusterModule(context.Background(), &proto.StartClusterModuleRequest{
		// 	ClientUrls: clientURLS,
		// 	PeerUrls:   peerURLS,
		// })
		// util.Check(err)
	},
}
