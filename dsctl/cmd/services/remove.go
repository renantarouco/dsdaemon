package services

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/proto"
)

var removeCmd = &cobra.Command{
	Use:     "remove",
	Aliases: []string{"rm"},
	Short:   "Removes services from the daemon's repository.",
	Args:    cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		for _, name := range args {
			_, err := client.RemoveService(context.Background(), &proto.RemoveServiceRequest{
				Name: name,
			})
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			fmt.Println(name, "removed")
		}
	},
}
