package services

import (
	"github.com/spf13/cobra"
)

var killInstanceCmd = &cobra.Command{
	Use:   "kill",
	Short: "Kills a service instance.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// instanceName := args[0]
		// _, err := client.KillInstance(context.Background(), &proto.KillInstanceRequest{
		// 	Name: instanceName,
		// })
		// util.Check(err)
	},
}
