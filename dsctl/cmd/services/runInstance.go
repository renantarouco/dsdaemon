package services

import (
	"github.com/spf13/cobra"
)

func init() {
	runInstanceCmd.Flags().StringP(
		"type",
		"t",
		"",
		"The service type to instantiate.")
	runInstanceCmd.Flags().StringSliceP(
		"port",
		"p",
		[]string{},
		"The ports mapping for the instance.")
	runInstanceCmd.MarkFlagRequired("type")
}

var runInstanceCmd = &cobra.Command{
	Use:   "run",
	Short: "Runs a service instance.",
	Run: func(cmd *cobra.Command, args []string) {
		// serviceName := cmd.Flag("type").Value.String()
		// ports, err := cmd.Flags().GetStringSlice("port")
		// util.Check(err)
		// portsMap := map[string]string{}
		// for _, portMap := range ports {
		// 	portArray := strings.Split(portMap, ":")
		// 	if len(portArray) != 2 {
		// 		log.Println("wrong port mapping syntax - instance:host")
		// 		os.Exit(1)
		// 	}
		// 	portsMap[portArray[0]] = portArray[1]
		// }
		// _, err = client.RunInstance(context.Background(), &proto.RunInstanceRequest{
		// 	ServiceName: serviceName,
		// 	PortsMap:    portsMap,
		// })
		// util.Check(err)
	},
}
