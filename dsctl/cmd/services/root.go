package services

import (
	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/dsctl/util"
	"gitlab.com/dsnescafe/dsdaemon/proto"
	"google.golang.org/grpc"
)

const (
	chunkSize    = 64 * 1024
	servicesPort = "9001"
)

var (
	conn   *grpc.ClientConn
	client proto.ServicesModuleClient
)

func init() {
	ServiceCmd.AddCommand(pushCmd)
	ServiceCmd.AddCommand(listCmd)
	ServiceCmd.AddCommand(removeCmd)
	ServiceCmd.AddCommand(pullCmd)
	ServiceCmd.AddCommand(runInstanceCmd)
	ServiceCmd.AddCommand(listInstancesCmd)
	ServiceCmd.AddCommand(killInstanceCmd)
}

// ServiceCmd - The root command for 'dsctl service|srv ...'
var ServiceCmd = &cobra.Command{
	Use:     "services",
	Aliases: []string{"srv"},
	Short:   "Manages daemon's services.",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		daemonIP := cmd.Flag("daemon").Value.String()
		var err error
		conn, err = grpc.Dial(daemonIP+":"+servicesPort, grpc.WithInsecure())
		util.Check(err)
		client = proto.NewServicesModuleClient(conn)
		util.Check(err)
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		conn.Close()
	},
}
