package services

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/dsctl/util"
	"gitlab.com/dsnescafe/dsdaemon/proto"
)

func init() {
	pullCmd.Flags().StringP(
		"output",
		"o",
		".",
		"Output directory do download the service file.")
}

var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "Downloads a service from the daemon's repository.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		stream, err := client.PullService(context.Background(), &proto.PullServiceRequest{
			Name: name,
		})
		util.Check(err)
		var fileName string
		var file *os.File
		var chunk *proto.ServiceChunk
		fmt.Println("started pull")
		for {
			chunk, err = stream.Recv()
			if err != nil {
				if err == io.EOF {
					err = nil
					file.Close()
					fmt.Println("finished pull")
					break
				}
				break
			}
			chunkNumber := chunk.Header.GetChunkNumber()
			if chunkNumber == 1 {
				fileName = chunk.Header.GetFileName()
				outputDir, err := filepath.Abs(cmd.Flag("output").Value.String())
				util.Check(err)
				filePath := filepath.Join(outputDir, fileName)
				_, err = os.Stat(filePath)
				if err != nil && !os.IsNotExist(err) {
					util.Check(err)
				}
				file, err = os.Create(filePath)
				util.Check(err)
				file, err = os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
				util.Check(err)
			}
			_, err = file.Write(chunk.GetChunk())
			file.Sync()
			fmt.Printf("%s - %d/%d\n",
				fileName,
				chunkNumber,
				chunk.Header.GetTotalChunks())
		}
	},
}
