package services

import (
	"context"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/dsctl/util"
	"gitlab.com/dsnescafe/dsdaemon/proto"
)

var pushCmd = &cobra.Command{
	Use:   "push",
	Short: "Installs a service in the daemon's repository.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fileRelPath := args[0]
		filePath, err := filepath.Abs(fileRelPath)
		util.Check(err)
		fileStat, err := os.Stat(filePath)
		util.Check(err)
		fileSize := fileStat.Size()
		chunksDiv := math.Ceil(float64(fileSize) / float64(chunkSize))
		totalChunks := uint32(chunksDiv)
		stream, err := client.PushService(context.Background())
		util.Check(err)
		file, err := os.Open(filePath)
		util.Check(err)
		buffer := make([]byte, chunkSize)
		_, fileName := filepath.Split(filePath)
		chunkNumber := uint32(1)
		chunk := &proto.ServiceChunk{
			Header: &proto.ChunkHeader{
				FileName:    fileName,
				TotalChunks: totalChunks,
			},
		}
		fmt.Println("started push")
		for {
			bytesRead, err := file.Read(buffer)
			if err != nil {
				if err == io.EOF {
					fmt.Println("finished push")
					break
				}
				fmt.Println(err.Error())
				return
			}
			chunk.Header.ChunkNumber = chunkNumber
			chunk.Chunk = buffer[:bytesRead]
			err = stream.Send(chunk)
			util.Check(err)
			fmt.Printf("%s - %d/%d - %d bytes\n", fileName, chunkNumber, totalChunks, bytesRead)
			chunkNumber++
		}
		_, err = stream.CloseAndRecv()
		util.Check(err)
	},
}
