package services

import (
	"context"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"gitlab.com/dsnescafe/dsdaemon/dsctl/util"
	"gitlab.com/dsnescafe/dsdaemon/proto"
)

var listCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "Lists all services installed in the daemon.",
	Run: func(cmd *cobra.Command, args []string) {
		stream, err := client.ListServices(context.Background(), &proto.ListServicesRequest{})
		util.Check(err)
		for {
			service, err := stream.Recv()
			if err != nil {
				if err == io.EOF {
					break
				}
				util.Check(err)
			}
			fmt.Printf("%s - %v - %d bytes\n",
				service.GetName(),
				service.GetPorts(),
				service.GetSize())
		}
	},
}
