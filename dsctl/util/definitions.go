package util

const (
	// ChunkSize - Size to split large files or messages.
	ChunkSize = 64 * 1024
)
