package util

import (
	"fmt"
	"os"
)

// Check - Utility function that prints an error and exits the program.
func Check(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
